import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Blog } from 'src/app/models/Blog';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  blogUrl:string = 'http://localhost:8081/api/users'

  constructor(private http:HttpClient) { }

  getBlogs():Observable<Blog[]> {
    return this.http.get<Blog[]>(this.blogUrl);
  }
}
