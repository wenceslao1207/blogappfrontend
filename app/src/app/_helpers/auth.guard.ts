import { Injectable } from '@angular/core';
import { Router, CanActive, ActivatedRoouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service.ts';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActive {
  constructor (
    private router: Router,
    private authService: AuthService
  ) {}

  canActive(route: ActivatedRoouteSnapshot, state: RouterStateSnapshot):void {
    const currentUser = this.authService.currentUser;
    if (currentUser) {
      return true;
    }

    this.router.navigate(['/login'], {queryParams: {
      returnUrl: state.url
    }});
  }

}
