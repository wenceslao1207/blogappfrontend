import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

	title:string = 'Hakunamatata';

	constructor() {
		console.log(this.title);
		this.changeTitle("Blog App")
	}

	changeTitle(title:string):void {
		this.title = title
	}
}
