export class Blog {
  id: number;
  title: string;
  body: string;
  author: string;
  creationDate: string;
  updateDate: string;
}
