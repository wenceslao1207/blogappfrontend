up:
	docker-compose up 

down:
	docker-compose down

stop:
	docker-compose stop

yarn:
	docker exec -ti HAKUNAMATATA_node yarn $(filter-out $@,$(MAKECMDGOALS))

logs:
	docker-compose logs -f

ng:
	docker exec -ti HAKUNAMATATA_node /home/node/.yarn/bin/ng $(filter-out $@,$(MAKECMDGOALS))
